import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime

class SL_gen(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()
        now = datetime.now()
        self.timedate = now.strftime("%d%m - %H%M")

    def test_SLgen_success(self):
            # step 1: login
            stamp = self.timedate
            browser = self.browser
            browser.maximize_window()
            browser.get("https://main-dev.kedai-sayur.com/security/sign-in")
            browser.find_element(By.CLASS_NAME,'ant-btn').click()
            time.sleep(2)

            #step 2 : navigate to SL page
            WebDriverWait(browser,10).until(EC.element_to_be_clickable((By.XPATH,"/html/body/div[1]/section/aside/div[1]/ul/li[5]/div/span[2]"))).click() #procurement
            time.sleep(1)
            browser.find_element(By.XPATH,"/html/body/div[1]/section/aside/div[1]/ul/li[5]/ul/li[5]/div/span").click() #buyer
            # WebDriverWait(browser,3).until(EC.element_to_be_clickable((By.XPATH,"/html/body/div[1]/section/aside/div[1]/ul/li[5]/ul/li[5]/ul/li[3]"))).click()
            time.sleep(3)

            element = browser.find_element(By.XPATH,"/html/body/div[1]/section/aside/div[1]/ul/li[5]/ul/li[5]/ul/li[3]")#shopping list
            ActionChains(browser).scroll_to_element(element).perform()
            WebDriverWait(browser,5).until(EC.element_to_be_clickable((By.XPATH,"/html/body/div[1]/section/aside/div[1]/ul/li[5]/ul/li[5]/ul/li[3]"))).click()
            # browser.find_element(By.XPATH,"/html/body/div[1]/section/aside/div[1]/ul/li[5]/ul/li[5]/ul/li[3]").click() #shopping list
            time.sleep(5)
            # /html/body/div[3]/div/div/div/div[2]/div[3]/button[1]/span

            #step3: generate SL
            WebDriverWait(browser,20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/section/section/main/div/div/div[2]/div/div/div/div/div/div/div[1]/div/div/div[1]/button/span[2]"))).click()
            browser.find_element(By.XPATH, "//*[@id='txtRemarkGenerateShoppingList']").send_keys("autoEC-", stamp)
            time.sleep(1)
            browser.find_element(By.XPATH, "/html/body/div[3]/div/div/div/div[2]/div[3]/button[1]/span").click()
            time.sleep(5)
            # browser.find_element(By.CLASS_NAME, "ant-btn-primary").click()
            # create_web_element("modal")
            # modal = browser.findElement(By.XPATH("/html/body/div[5]/div/div[2]/div"))
            # child = modal.findElement(By.XPATH(".//html/body/div[5]/div/div[2]/div/div[2]/div/div/div[2]/button[2]"))
            # child.click()
            # WebDriverWait(browser,10).until(EC.element_to_be_clickable((By.XPATH,"/html/body/div[5]/div/div[2]/div/div[2]/div/div/div[2]/button[2]"))).click()
            WebDriverWait(browser,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button.ant-btn-primary:nth-child(2)"))).click()
            time.sleep(15)
    
    def tearDown(self):
        self.browser.quit()


if __name__ == "__main__":
    unittest.main()